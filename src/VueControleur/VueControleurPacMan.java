package VueControleur;

import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import modele.Direction;
import modele.Fantome;
import modele.Jeu;
import modele.Pacman;
import modele.Murs;
import modele.PacGommes;
import modele.SuperGomme;


/** Cette classe a deux fonctions :
 *  (1) Vue : proposer une représentation graphique de l'application (cases graphiques, etc.)
 *  (2) Controleur : écouter les évènements clavier et déclencher le traitement adapté sur le modèle (flèches direction Pacman, etc.))
 *
 * @author freder
 */
public class VueControleurPacMan extends JFrame implements Observer {

    private Jeu jeu; // référence sur une classe de modèle : permet d'accéder aux données du modèle pour le rafraichissement, permet de communiquer les actions clavier (ou souris)

    private int sizeX; // taille de la grille affichée
    private int sizeY;

    private ImageIcon icoPacMan; // icones affichées dans la grille
    private ImageIcon icoFantome;
    private ImageIcon icoCouloir;
    private ImageIcon icoMurX;
    private ImageIcon icoMurY;
    private ImageIcon icoGomme;
    private ImageIcon icoMur;
    private ImageIcon icoSuperGomme;
    private ImageIcon icoSuperFantome;
    
    private String scoreStr;
    private String highScoreStr;
    private JLabel scoreJLab;
    private JLabel highScoreJLab;

    private JLabel[][] tabJLabel; // cases graphique (au moment du rafraichissement, chaque case va être associé à une icône, suivant ce qui est présent dans la partie modèle)
   
    public VueControleurPacMan(int _sizeX, int _sizeY) {

        sizeX = _sizeX;
        sizeY = _sizeY;

        chargerLesIcones();
        placerLesComposantsGraphiques();

        ajouterEcouteurClavier();
    }

    private void ajouterEcouteurClavier() {

        addKeyListener(new KeyAdapter() { // new KeyAdapter() { ... } est une instance de classe anonyme, il s'agit d'un objet qui correspond au controleur dans MVC
            @Override
            public void keyPressed(KeyEvent e) {
                
                switch(e.getKeyCode()) {  // on écoute les flèches de direction du clavier
                    case KeyEvent.VK_LEFT : jeu.getPacman().setDirection(Direction.gauche); break;
                    case KeyEvent.VK_RIGHT : jeu.getPacman().setDirection(Direction.droite); break;
                    case KeyEvent.VK_DOWN : jeu.getPacman().setDirection(Direction.bas); break;
                    case KeyEvent.VK_UP : jeu.getPacman().setDirection(Direction.haut); break;
                } 
            }
        });
    }

    public void setJeu(Jeu _jeu) {
        jeu = _jeu;
    }

    private void chargerLesIcones() {                       //chargement de l'image des icônes
        icoPacMan = chargerIcone("Images/Pacman.png");
        icoCouloir = chargerIcone("Images/Couloir.png");
        icoFantome = chargerIcone("Images/Fantome.png");
        icoGomme = chargerIcone("Images/PacGomme.png");
        icoMur = chargerIcone("Images/Mur.png"); 
        icoSuperGomme = chargerIcone("Images/SuperGomme.png"); 
        icoSuperFantome = chargerIcone("Images/SuperFantome.png");
    }

    private ImageIcon chargerIcone(String urlIcone) {

        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(urlIcone));
        } catch (IOException ex) {
            Logger.getLogger(VueControleurPacMan.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return new ImageIcon(image);
    }

    private void placerLesComposantsGraphiques() {

        setTitle("PacMan");
        setSize(900, 900);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // permet de terminer l'application à la fermeture de la fenêtre

        JComponent grilleJLabels = new JPanel(new GridLayout(sizeX, sizeY)); // grilleJLabels va contenir les cases graphiques et les positionner sous la forme d'une grille

        tabJLabel = new JLabel[sizeX][sizeY];
        
        for (int x = 0; x < sizeX-1; x++) {
            for (int y = 0; y < sizeY-1; y++) {

                JLabel jlab = new JLabel();
                tabJLabel[y][x] = jlab; // on conserve les cases graphiques dans tabJLabel pour avoir un accès pratique à celles-ci (voir mettreAJourAffichage() )
                grilleJLabels.add(jlab);
            }
        }
        scoreJLab = new JLabel();
        highScoreJLab = new JLabel();

        grilleJLabels.add(scoreJLab);
        grilleJLabels.add(highScoreJLab);
        add(grilleJLabels);
    }

    
    /**
     * Il y a une grille du côté du modèle ( jeu.getGrille() ) et une grille du côté de la vue (tabJLabel)
     */
    private void mettreAJourAffichage() {

        for (int x = 0; x < sizeX-1; x++) {
            for (int y = 0; y < sizeY-1; y++) {
                if (jeu.getGrille()[x][y] instanceof Pacman) { // si la grille du modèle contient un Pacman, on associe l'icône Pacman du côté de la vue
                    tabJLabel[x][y].setIcon(icoPacMan);  
                } else if (jeu.getGrille()[x][y] instanceof Fantome && jeu.superMode==false) {  // si la grille du modèle contient un Fantome et que le supermode est désactivé 
                    tabJLabel[x][y].setIcon(icoFantome);                                        //   alors on associe l'icône Fantome du côté vue                                                              
                } else if (jeu.getGrille()[x][y] instanceof Fantome && jeu.superMode==true) {   // si la grille du modèle contient un Fantome et que le supermode est activé
                    tabJLabel[x][y].setIcon(icoFantome);                                        //   alors on associe l'icône superFantome du côté vue  
                    tabJLabel[x][y].setIcon(icoSuperFantome);
                } else if (jeu.getGrille()[x][y] instanceof SuperGomme) { 
                    tabJLabel[x][y].setIcon(icoSuperGomme);
                } else if (jeu.getGrille()[x][y] instanceof PacGommes) { 
                    tabJLabel[x][y].setIcon(icoGomme);
                } else if (jeu.getGrille()[x][y]instanceof Murs) {
                    tabJLabel[x][y].setIcon(icoMur);
                } else {                                                             
                    tabJLabel[x][y].setIcon(icoCouloir);           //Sinon si il n'y pas d'entité, on affiche l'image d'un couloir 
                }
            }
        }
        scoreStr = Integer.toString(jeu.getScore());
        highScoreStr = Integer.toString(jeu.getHighScore());
        scoreJLab.setText("Score : "+scoreStr);
        highScoreJLab.setText("High : "+highScoreStr);
    }

    @Override
    public void update(Observable o, Object arg) {
        
        
        mettreAJourAffichage();
        
        SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        mettreAJourAffichage();
                    }
                });       
    }

}