/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;
import java.awt.Point;
import java.util.HashMap;
import java.lang.Math;

import java.util.Random;

/**
 *
 * @author freder
 */
public class Fantome extends Entite {

    private Random r = new Random();

    public Fantome(Jeu _jeu) {
        super(_jeu);
    }

    @Override
    public void choixDirection() {
        
        Pacman pm = this.jeu.getPacman();
        Point posPac = this.jeu.getMap().get(pm);
        double x_p = posPac.getX();
        double y_p = posPac.getY();
         
        Point posFan = this.jeu.getMap().get(this);
        double x_f = posFan.getX();
        double y_f = posFan.getY();
        
        double dif_x = x_f - x_p;
        double dif_y = y_f - y_p;
        
        if((Math.abs(dif_x)<Math.abs(dif_y)) && dif_x!=0 && dif_x<0) {
            if(!jeu.superMode && !jeu.detecteMur(this, d.droite))
                d = Direction.droite;
            else if (jeu.superMode && !jeu.detecteMur(this, d.gauche))
                d = Direction.gauche;
            else
                d = randomDirection() ;
        } else if ((Math.abs(dif_x)<Math.abs(dif_y)) && dif_x!=0 && dif_x>0) {
            if(!jeu.superMode && !jeu.detecteMur(this, d.gauche))
                d = Direction.gauche;
            else if (jeu.superMode && !jeu.detecteMur(this, d.droite))
                d = Direction.droite;
            else
                d = randomDirection() ;
        }
        else if ((Math.abs(dif_y)<Math.abs(dif_x)) && dif_y!=0 && dif_y<0) {
            if(!jeu.superMode && !jeu.detecteMur(this, d.haut))
                d = Direction.haut;
            else if (jeu.superMode && !jeu.detecteMur(this, d.bas))
                d = Direction.bas;
            else
                d = randomDirection() ;
        } else if ((Math.abs(dif_y)<Math.abs(dif_x)) && dif_y!=0 && dif_y>0) {
            if(!jeu.superMode && !jeu.detecteMur(this, d.bas))
                d = Direction.bas;
            else if (jeu.superMode && !jeu.detecteMur(this, d.haut))
                d = Direction.haut;
            else
                d = randomDirection() ;
        } else if((Math.abs(dif_x)<Math.abs(dif_y)) && dif_x==0 && dif_y<0) {
            if(!jeu.superMode && !jeu.detecteMur(this, d.bas))
                d = Direction.bas;
            else if (jeu.superMode && !jeu.detecteMur(this, d.haut))
                d = Direction.haut;
            else
                d = randomDirection() ;
        } else if ((Math.abs(dif_x)<Math.abs(dif_y)) && dif_x==0 && dif_y>0) {
            if(!jeu.superMode && !jeu.detecteMur(this, d.haut))
                d = Direction.haut;
            else if (jeu.superMode && !jeu.detecteMur(this, d.bas))
                d = Direction.bas;
            else
                d = randomDirection() ;
        } else if ((Math.abs(dif_y)<Math.abs(dif_x)) && dif_y==0 && dif_x<0) {
            if(!jeu.superMode && !jeu.detecteMur(this, d.droite))
                d = Direction.droite;
            else if (jeu.superMode && !jeu.detecteMur(this, d.gauche))
                d = Direction.gauche;
            else
                d = randomDirection() ;
        } else if ((Math.abs(dif_y)<Math.abs(dif_x)) && dif_y==0 && dif_x>0) {
            if(!jeu.superMode && !jeu.detecteMur(this, d.gauche))
                d = Direction.gauche;
            else if (jeu.superMode && !jeu.detecteMur(this, d.droite))
                d = Direction.droite;
            else
                d = randomDirection() ;
        } else
            d = randomDirection() ;
    }
    
    public Direction randomDirection() {
        switch (r.nextInt(4)) {
                case 0:
                    d = Direction.droite;
                    break;
                case 1:
                    d = Direction.bas;
                    break;

                case 2:
                    d = Direction.haut;
                    break;

                case 3:
                    d = Direction.gauche;
                    break;
        }
        
        return d;
    }
}
