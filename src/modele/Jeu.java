/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.awt.Point;
import java.util.HashMap;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Random;
import modele.Music;

/** La classe Jeu a deux fonctions 
 *  (1) Gérer les aspects du jeu : condition de défaite, victoire, nombre de vies
 *  (2) Gérer les coordonnées des entités du monde : déplacements, collisions, perception des entités, ... 
 *
 * @author freder
 */
public class Jeu extends Observable implements Runnable {

    public static final int SIZE_X = 11;
    public static final int SIZE_Y = 11 ;
    public int Score;
    public int HighScore = 0;
    public int NbPacGommes;
    public boolean superMode = false;
    

    private Pacman pm;
    private Fantome f;
    private Fantome f1;
    private PacGommes p;
    private SuperGomme sp; 
    private SuperGomme sp1;

    private HashMap<Entite, Point> map = new  HashMap<Entite, Point>(); // permet de récupérer la position d'une entité à partir de sa référence
    private Entite[][] grilleEntites = new Entite[SIZE_X][SIZE_Y]; // permet de récupérer une entité à partir de ses coordonnées
     
    
    public Jeu() {
        initialisationDesEntites();   
    }
    
    public Entite[][] getGrille() {
        return grilleEntites;
    }
    
    public HashMap<Entite, Point> getMap() {
        return map;
    }
    
    public Pacman getPacman() {
        return pm;
    }
    
    /** Crée un nouveau fantôme à une position aléatoire dans la grille */
    private void initNVfantome(){
        
        Random random = new Random();
        int x;
        int y;
        
        do {
            x = random.nextInt(8);
            if(x < 1){
                x=x+1;
            }

            y = random.nextInt(8);
            if(y < 1){
                y=y+1;
            }
        } while(grilleEntites[x][y]!=null);
          
        map.put(f, new Point(x,y));
        grilleEntites[x][y]=f;
    }
    
    private void initialisationDesEntites() {
        
        pm = new Pacman(this);
        map.put(pm, new Point(5, 5));
        
        f = new Fantome(this);
        map.put(f, new Point(1, 1));
        
        f1 = new Fantome(this);
        map.put(f1, new Point(1,7));
        
        /** Positionne les murs*/
        initMurs();
        
        /**Crée et pose les PacGomme dans toutes les cases inocuppées*/
        p = new PacGommes(this);
        for(int x=0; x<SIZE_X; x++) {
            for(int y=0; y<SIZE_Y; y++) {
                if(grilleEntites[x][y]== null) {
                    map.put(p, new Point(x,y));
                }
            }
        }
        
        /** Crée 2 SuperGomme à deux positions différentes*/
        sp = new SuperGomme(this);
        grilleEntites[2][6] = sp;
        map.put(sp, new Point(2,6));
        
        sp1 = new SuperGomme(this);
        grilleEntites[8][2] = sp1;
        map.put(sp1, new Point(8,2));
        
        /**Initialise tout les paramètres en début de partie*/
        reset();
    }
    
    private void initMurs(){
        
        Murs m = new Murs(this);
        
        //Positionne les murs autour du terrain
        
        for(int y=0; y< SIZE_Y-1; y++){
            grilleEntites[0][y] = m;
            map.put(m, new Point(0,y));
        }
        for(int y=0; y< SIZE_Y; y++){        
            grilleEntites[9][y] = m;
            map.put(m, new Point(9,y));
        }
        
        for(int x=0; x< SIZE_X; x++){        
            grilleEntites[x][0] = m;
            map.put(m, new Point(x,0));
        }
        for(int x=0; x< SIZE_X; x++){        
            grilleEntites[x][9] = m;
            map.put(m, new Point(x,9));
        }
        
        //Positionne les murs-obstacles dans le terrain
        
        grilleEntites[1][4] = m;
        map.put(m, new Point(1,4));
        
        grilleEntites[1][8] = m;
        map.put(m, new Point(1,8));
        grilleEntites[2][8] = m;
        map.put(m, new Point(2,8));
        
        for(int x=2; x<5; x++){
            grilleEntites[x][2] = m;
            map.put(m, new Point(x,2));
        }
        grilleEntites[3][3] = m;
        map.put(m, new Point(3,3));
        
        grilleEntites[3][5] = m;
        map.put(m, new Point(3,5));
        grilleEntites[2][6] = m;
        map.put(m, new Point(2,6));
        grilleEntites[3][6] = m;
        map.put(m, new Point(3,6));
        
        grilleEntites[6][2] = m;
        map.put(m, new Point(6,2));
        grilleEntites[7][2] = m;
        map.put(m, new Point(7,2));
        
        for(int x=5; x<8;x++){
            grilleEntites[x][4] = m;
            map.put(m, new Point(x,4));
        }
        grilleEntites[7][5] = m;
        map.put(m, new Point(7,5));
        
        grilleEntites[5][6] = m;
        map.put(m, new Point(5,6));
        grilleEntites[5][7] = m;
        map.put(m, new Point(5,7));
        
        grilleEntites[8][7] = m;
        map.put(m, new Point(8,7));
        grilleEntites[8][8] = m;
        map.put(m, new Point(8,8));
        grilleEntites[7][8] = m;
        map.put(m, new Point(7,8));
    }
    
    private void reset() {
        
        Score=0;
        
        //Nb initial de gommes (en comptant les SuperGommes
        NbPacGommes=41;
        
        System.out.println("reset, Score : " + Score);
        System.out.println("reset, HighScore : " + HighScore);    
         
        //vide la grille
        for(int x=1; x<SIZE_X-1; x++) {
            for(int y=1; y<SIZE_Y-1; y++) {
                if(!(grilleEntites[x][y]instanceof Murs)){
                    grilleEntites[x][y]=null;
                }
            }
        }
        
        //repositionne toutes les entités
        grilleEntites[5][5] = getPacman();
        map.put(pm, new Point(5, 5));
        
        grilleEntites[1][1] = f;
        map.put(f, new Point(1, 1));
        
        grilleEntites[1][7] = f1;
        map.put(f1, new Point(1, 7));
        
        for(int x=0; x<SIZE_X; x++) {
            for(int y=0; y<SIZE_Y; y++) {
                if(grilleEntites[x][y]== null) {
                    grilleEntites[x][y] = p;
                }
            }
        }
        
        grilleEntites[2][6] = sp;
        map.put(sp, new Point(2,6));
        
        grilleEntites[8][2] = sp1;
        map.put(sp1 , new Point(8,2));
    }
         
    
    /** Permet a une entité  de percevoir sont environnement proche et de définir sa strétégie de déplacement 
     * (fonctionalité utilisée dans choixDirection() de Fantôme)
     */
    public Object regarderDansLaDirection(Entite e, Direction d) {
        Point positionEntite = map.get(e);
        return objetALaPosition(calculerPointCible(positionEntite, d));
    }
    
    /** Si le déclacement de l'entité est autorisé (pas de mur ou autre entité), il est réalisé
     */
    public boolean deplacerEntite(Entite e, Direction d) {
        
        boolean retour;
        
        Point pCourant = map.get(e);
        
        Point pCible = calculerPointCible(pCourant, d);
        
        //Cas où le Pacman mange une PacGomme, incrémente le score et highscore si nécéssaire et relance une partie si la dernière gomme est mangée
        if (contenuDansGrille(pCible) && objetALaPosition(pCible)instanceof PacGommes && objetALaPosition(pCourant)instanceof Pacman) {
            deplacerEntite(pCourant, pCible, e);
            //System.out.println("deplacerEntite : " + Score);
            addScore(1);
            if (Score > HighScore)
                HighScore = Score;
            
            if(NbPacGommes==0){
                reset();
            }
            //System.out.println("deplacerEntite++ : " + Score);
            
            retour = true;
        } 
        //Cas où le Pacman mange une SuperGomme, incrémente le score et highscore si nécéssaire et passe en mode SuperMode
        else if (contenuDansGrille(pCible) && objetALaPosition(pCible)instanceof SuperGomme && objetALaPosition(pCourant)instanceof Pacman) {
            deplacerEntite(pCourant, pCible, e);
            //System.out.println("deplacerEntite : " + Score);
            addScore(10);
            
            if (Score > HighScore)
                HighScore = Score;
            
            activeSuper();
            
            if(NbPacGommes==0){
                reset();
            }
            //System.out.println("deplacerEntite++ : " + Score);
            
            retour = true;
        } 
        //Cas où la case est vide, déplace simplement le Pacman
        else if (contenuDansGrille(pCible) && objetALaPosition(pCible)==null && objetALaPosition(pCourant)instanceof Pacman) {
            deplacerEntite(pCourant, pCible, e);
            retour = true;   
        } 
        //Cas de collision avec le fantôme, mange le fantome et incrémente le score de 100 si le SUperMode est actif, relance une partie sinon
        else if (contenuDansGrille(pCible) && objetALaPosition(pCible)instanceof Fantome && objetALaPosition(pCourant)instanceof Pacman) {
            if(superMode == false) {
                reset();
                retour = true;
            }
            
            else {
                deplacerEntite(pCourant, pCible, e);
                addScore();
                
                if (Score > HighScore)
                    HighScore = Score;
                
                retour = true;                           
            }
        } 
        //Même chose que le cas précédent mais dans le cas où le fantome mange le Pacman
        else if (contenuDansGrille(pCible) && objetALaPosition(pCible)instanceof Pacman && objetALaPosition(pCourant)instanceof Fantome) {
            if(superMode == false) {
                reset();
                retour = true;
            }
            
            else {
                deplacerEntite(pCourant, pCible, e);
                addScore();
                
                if (Score > HighScore)
                    HighScore = Score;
                
                retour = true;                          
            }  
        }
        //Cas de déplacement du fantome
        else if (contenuDansGrille(pCible) && (objetALaPosition(pCible)instanceof PacGommes || objetALaPosition(pCible)==null) && objetALaPosition(pCourant)instanceof Fantome) {
            deplacerEntite(pCourant, pCible, e);
            retour = true;
        } 
        //Autres cas
        else {
            retour = false;
        }
        
        return retour;
    }
    
    
    private Point calculerPointCible(Point pCourant, Direction d) {
        Point pCible = null;
        
        switch(d) {
            case haut: pCible = new Point(pCourant.x, pCourant.y - 1); break;
            case bas : pCible = new Point(pCourant.x, pCourant.y + 1); break;
            case gauche : pCible = new Point(pCourant.x - 1, pCourant.y); break;
            case droite : pCible = new Point(pCourant.x + 1, pCourant.y); break;     
        }
        return pCible;
    }
    
    private void deplacerEntite(Point pCourant, Point pCible, Entite e) {
        
        //Cas où le fantome veut se déplacer sur une case contenant une PacGomme, on déplace le fantome et remet une gomme dans la case
        if (objetALaPosition(pCible)instanceof PacGommes && objetALaPosition(pCourant)instanceof Fantome) {
            PacGommes p = new PacGommes(this);
            grilleEntites[pCourant.x][pCourant.y] =p;
            grilleEntites[pCible.x][pCible.y] = e;
            map.put(e, pCible);
        } 
        //Même chose que précédemment mais dans le cas où la case contient une SuperGomme
        else if(objetALaPosition(pCible)instanceof SuperGomme && objetALaPosition(pCourant)instanceof Fantome){
           
            SuperGomme sp = new SuperGomme(this);
            grilleEntites[pCourant.x][pCourant.y] = sp;
            grilleEntites[pCible.x][pCible.y] = e;
            map.put(e, pCible);
        
        } 
        //Cas où le SuperMode est actif et le Pacman mange un fantome
        else if(objetALaPosition(pCible)instanceof Fantome && objetALaPosition(pCourant)instanceof Pacman && superMode){
           
            grilleEntites[pCourant.x][pCourant.y] = null;
            grilleEntites[pCible.x][pCible.y] = e;
            
            map.put(e, pCible);
            
            /*Timer chrono = new Timer();
        
                chrono.schedule(new TimerTask() {
                    @Override
                    public void run(){*/
                        initNVfantome();
                    /*}
                }, 2000);*/
        } 
        //Même chose que précédemment mais dans le cas où le fantome se fait manger
        else if(objetALaPosition(pCible)instanceof Pacman && objetALaPosition(pCourant)instanceof Fantome && superMode){
           
            grilleEntites[pCible.x][pCible.y] = e;
            grilleEntites[pCourant.x][pCourant.y] = null;
            
            map.put(e, pCible);
            
            /*Timer chrono = new Timer();
        
                chrono.schedule(new TimerTask() {
                    @Override
                    public void run(){*/
                        initNVfantome();
                    /*}
                }, 2000);*/
        } 
        //Autres cas
        else {
            grilleEntites[pCourant.x][pCourant.y] = null;
            grilleEntites[pCible.x][pCible.y] = e;
            map.put(e, pCible);
        }
    }
    
    /** Vérifie que p est contenu dans la grille
     */
    private boolean contenuDansGrille(Point p) {
        return p.x >= 0 && p.x < SIZE_X && p.y >= 0 && p.y < SIZE_Y;
    }
    
    private Object objetALaPosition(Point p) {
        
        Object retour = null;
        
        if (contenuDansGrille(p)) {
            retour = grilleEntites[p.x][p.y];
        }
        
        return retour;
    }
    
    /**Incrémente le score d'un montant s (cas où on mange une gomme ou super gomme*/
    public int addScore(int s) {
        
        if(NbPacGommes > 0) {
            Score+=s;
            System.out.println("Score : " + Score);
            NbPacGommes-=1; 
      
        }
        
        return Score;
    }
    
    /**Incrémente le score de 100 (cas où on mange un fantome en mode super*/
    public int addScore() {
        
        Score+=100;
        System.out.println("Score : " + Score);
        
        return Score;
    }
    
    public int getScore() {
        return Score;
    }
    
     public int getHighScore() {
        return HighScore;
    }
    
     /**Regarde dans la direction où l'entite se deplace et retourne true si il y a un mur et faut sinon
      * est utilisé pour l'IA du fantome 
      */
    public boolean detecteMur(Entite e, Direction d) {
        
        Object test = regarderDansLaDirection(e,d);
        
        if (test instanceof Murs)
            return true;
        else
            return false;
    }
    
    /**Active le mode Super pendant 10 secondes*/
    public void activeSuper() {
        
        superMode=true;
        
        Timer chrono = new Timer();
        
        chrono.schedule(new TimerTask() {
            @Override
            public void run(){
                superMode=false;
            }
        }, 10000);   
    }
            
     
     /**
     * Un processus est créé et lancé, celui-ci execute la fonction run()
     */
    public void start() {
        new Thread(this).start();
    }

    @Override
    public void run() {
        /*if(grilleEntites[3][5]instanceof Murs) {
            System.out.println("Mur 3 5 ok");
        }*/
        
        while (true) {

            for (Entite e : map.keySet()) { // déclenchement de l'activité des entités, map.keySet() correspond à la liste des entités
                e.run(); 
            }

            setChanged();
            notifyObservers(); // notification de l'observer pour le raffraichisssement graphique

            try {
                Thread.sleep(500); // pause de 0.5s
            } catch (InterruptedException ex) {
                Logger.getLogger(Pacman.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}